//
//  SwiftCollections.swift
//

import UIKit

func random(in range: ClosedRange<Int>) -> Int
{
    return Int(arc4random_uniform(UInt32(range.upperBound - range.lowerBound))) + range.lowerBound
}
