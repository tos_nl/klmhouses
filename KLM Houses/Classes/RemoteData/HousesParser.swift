//
//  HousesParser.swift
//

import CoreData

class HousesParser {
    private struct Constant {
        struct Field {
            static let miniatures = "miniatures"
            static let number = "number"
            static let latitude = "latitude"
            static let longitude = "longitude"
            static let address = "address"
            static let description = "descriptionValue"
            static let name = "name"
        }
    }
    
    @discardableResult class func parseHouses(from parserData: [String: Any], to managedObjectContext: NSManagedObjectContext) -> [House] {
        var houses = [House]()
        
        guard let miniatures = parserData[Constant.Field.miniatures] as? [[String: Any]] else {
            return houses
        }
        
        for miniature in miniatures {
            guard let number = miniature[Constant.Field.number] as? Int16 else {
                continue
            }
            
            let house: House
            
            do {
                house = try NIManagedObjectManager.findOrCreate(House.self, withID: number, inManagedObjectContext: managedObjectContext)
            }
            catch (let error) {
                print("error getting house: \(error)")
                continue
            }
            
            house.latitude = (miniature[Constant.Field.latitude] as? Double) ?? 0
            house.longitude = (miniature[Constant.Field.longitude] as? Double) ?? 0
            house.address = miniature[Constant.Field.address] as? String
            
            if let description = miniature[Constant.Field.description] as? String {
                var nonEscapedString = description.replacingOccurrences(of: "\\n", with: "\n")
                nonEscapedString = nonEscapedString.replacingOccurrences(of: "\\\"", with: "\"")
                house.info = nonEscapedString
            }
            
            house.name = miniature[Constant.Field.name] as? String

            houses.append(house)
        }
        
        return houses
    }
}
