//
//  NIBaseManagedObject.swift
//

import CoreData

class NIBaseManagedObject : NSManagedObject
{
    enum BaseError : Error
    {
        case missingID
        case coreData
        case notFound
        case alreadyExisting
    }
    
    class var uniqueIDProperyName : String
    {
        fatalError("Must Override")
    }
}
