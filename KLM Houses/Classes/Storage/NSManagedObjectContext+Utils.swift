//
//  NSManagedObjectContext+Utils.swift
//

import CoreData
import UIKit

extension NSManagedObjectContext
{
    typealias PerformBlock = (NSManagedObjectContext) -> Void
    
    /* asynchronously performs the block on the context's queue.  Encapsulates an autorelease pool and a call to processPendingChanges */
    func ni_perform(_ block: @escaping PerformBlock)
    {
        self.perform
        {
            block(self)
        }
    }
    
    /* synchronously performs the block on the context's queue.  May safely be called reentrantly.  */
    func ni_performAndWait(_ block: @escaping PerformBlock)
    {
        self.performAndWait
        {
            block(self)
        }
    }
    
    /* asynchronously performs the block on the context's queue.  Encapsulates an and a call to processPendingChanges */
    func ni_performAndSaveRecursively(_ block: @escaping PerformBlock)
    {
        self.perform
        {
            block(self)
            
            self.ni_saveRecursively()
        }
    }
    
    typealias SavingBlock = (Error?) -> Void
    
    func ni_saveRecursively(_ completion: SavingBlock? = nil)
    {
        var identifier = UIBackgroundTaskInvalid
        
        identifier = UIApplication.shared.beginBackgroundTask(withName: nil)
        {
            identifier = UIBackgroundTaskInvalid
        }
        
        self.ni_performSaveRecursively
        {
            (error: Error?) in
            
            completion?(error)
         
            UIApplication.shared.endBackgroundTask(identifier)
        }
    }
    
    private func ni_performSaveRecursively(_ completion: SavingBlock? = nil)
    {
        self.ni_save
        {
            (error: Error?) in
            
            guard let parentContext = self.parent else
            {
                completion?(error)
                return
            }
            
            parentContext.ni_performSaveRecursively(completion)
        }
    }
    
    func ni_save(_ completion: SavingBlock? = nil)
    {
        self.perform
        {
            guard self.hasChanges else
            {
                print("nothing to save in MOC[\(self.name ?? "name not set")], ignoring")
                completion?(nil)
                return
            }
            
            var saveError: Error?
            
            do
            {
                try self.save()
            }
            catch (let error)
            {
                print("error saving context : \(error)")
                saveError = error
            }
            
            completion?(saveError)
        }
    }
}
