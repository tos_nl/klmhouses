
//
//  CoreDataStorage.swift
//

import CoreData
import UIKit

extension NSNotification.Name
{
    static let NIMainQueueContextDidMergeChanges = NSNotification.Name("CoreDataStorageMainQueueContextDidMergeChanges")
}

class NICoreDataStorage
{
    enum AccessType
    {
        case read
        case write
    }
    
    private let dataModelName: String
    private let bundleID: String?
    
    private var persistentStoreCoordinator: NSPersistentStoreCoordinator!
    
    init(modelName: String, bundle: String? = nil)
    {
        self.dataModelName = modelName
        self.bundleID = bundle
        
        NotificationCenter.default.addObserver(self, selector: #selector(saveByApplicationNotification(_:)), name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(saveByApplicationNotification(_:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        self.initializeCoreData()
    }
    
    // MARK: Init
    @discardableResult
    func initializeCoreData() -> NSManagedObjectContext
    {
        self.setupPersistentStoreCoordinator()
        // This initializes all properties
        return self.mainQueueContext
    }
    
    private func applicationDocumentsDirectory() -> URL
    {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else
        {
            fatalError("no documentDirectory")
        }
        
        return url
    }
    
    lazy var masterManagedObjectContext: NSManagedObjectContext =
    {
        let masterManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        masterManagedObjectContext.name = "masterManagedObjectContext"
        masterManagedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return masterManagedObjectContext
    }()
    
    lazy var mainQueueContext: NSManagedObjectContext =
    {
        let mainQueueContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainQueueContext.name = "mainQueueContext"
        mainQueueContext.performAndWait
        {
            mainQueueContext.parent = self.masterManagedObjectContext
            mainQueueContext.stalenessInterval = 0
            
            NotificationCenter.default.addObserver(self, selector: #selector(mergeChangesIntoMainQueueContextWithNotification(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: self.masterManagedObjectContext)
        }
        
        return mainQueueContext
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel =
    {
        var bundle: Bundle? = nil
        
        if let bundleID = self.bundleID
        {
            bundle = Bundle(identifier: bundleID)
        }
        else
        {
            bundle = Bundle.main
        }
        
        guard let modelURL = bundle?.url(forResource: self.dataModelName, withExtension: "momd"),
               let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else
        {
            fatalError("no such model")
        }
        
        return managedObjectModel
    }()
    
    private var databaseName: String
    {
        return "\(self.dataModelName).sqlite"
    }
    
    private var databaseURL: URL
    {
        return self.applicationDocumentsDirectory().appendingPathComponent(databaseName)
    }
    
    private var databaseOptions: [AnyHashable : Any]
    {
        return [NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true]
    }
    
    private var storeType: String
    {
        #if TESTING
            return NSInMemoryStoreType
        #else
            return NSSQLiteStoreType
        #endif
    }
    
    private func setupPersistentStoreCoordinator()
    {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
        guard (self.persistentStoreCoordinator == nil) else
        {
            return
        }
        
        self.persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        
        do
        {
            try self.persistentStoreCoordinator.addPersistentStore(ofType: storeType, configurationName: nil, at: databaseURL, options: databaseOptions)
        }
        catch (let error)
        {
            print("error adding persistent storage : \(error)")
            
            // Remove an old version and try again
            try? FileManager.default.removeItem(at: databaseURL)
            
            self.persistentStoreCoordinator = nil
            self.setupPersistentStoreCoordinator()
        }
    }
    
    // MARK: Notiifcations
    @objc
    private func saveByApplicationNotification(_ notification: Notification)
    {
        self.mainQueueContext.ni_saveRecursively()
    }
    
    @objc
    private func mergeChangesIntoMainQueueContextWithNotification(_ notification: Notification)
    {
        self.mainQueueContext.ni_perform
        {
            (managedObjectContext : NSManagedObjectContext) in
            
            managedObjectContext.mergeChanges(fromContextDidSave: notification)
            
            NotificationCenter.default.post(name: NSNotification.Name.NIMainQueueContextDidMergeChanges, object: nil)
        }
    }
}
