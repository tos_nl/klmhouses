//
//  ManagedObjectManager.swift
//

import CoreData

/**
    @brief
    This is a manager that can create or find NIBaseManagedObjects of given types with additional parameters.
    it gives more compile-time control over object types and exceptions
    In case of performing search by uniqueID, uniqueIDProperyName of given type is used
 */

class NIManagedObjectManager
{
    class func create<T : NSManagedObject>(_ type: T.Type, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> T
    {
        guard let entityDescription = NSEntityDescription.entity(forEntityName: NSStringFromClass(T.self), in: managedObjectContext) else
        {
            throw NIBaseManagedObject.BaseError.coreData
        }
        
        return T(entity: entityDescription, insertInto: managedObjectContext)
    }
    
    class func findOrCreate<T : NIBaseManagedObject>(_ type: T.Type, withID uniqueID: Any, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> T
    {
        do
        {
            return try self.findFirst(type, withValue: uniqueID, forKey: T.uniqueIDProperyName, inManagedObjectContext: managedObjectContext)
        }
        catch NIBaseManagedObject.BaseError.notFound
        {
            let newObject = try self.create(type, inManagedObjectContext: managedObjectContext)
            
            (newObject as NSManagedObject).setValue(uniqueID, forKey: T.uniqueIDProperyName)
            return newObject
        }
    }
    
    class func findFirst<T : NIBaseManagedObject>(_ type: T.Type, withID uniqueID: Any, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> T
    {
        return try self.findFirst(type, withValue: uniqueID, forKey: T.uniqueIDProperyName, inManagedObjectContext: managedObjectContext)
    }
    
    class func findFirst<T : NIBaseManagedObject>(_ type: T.Type, withValue valueObject : Any? = nil, forKey keyString : String? = nil, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> T
    {
        let predicate = self.predicate(forKey: keyString, withValue: valueObject)
        
        return try self.findFirst(type, predicate: predicate, inManagedObjectContext: managedObjectContext)
    }
    
    class func findFirst<T : NIBaseManagedObject>(_ type: T.Type, predicate : NSPredicate?, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> T
    {
        let fetchRequest = self.fetchRequestForType(type)
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = predicate
        
        let results : [T]
        
        do
        {
            results = try managedObjectContext.fetch(fetchRequest)
        }
        catch (let error)
        {
            print("error performing fetch request : \(error)")
            throw NIBaseManagedObject.BaseError.coreData
        }
        
        guard let storedObject = results.last else
        {
            throw NIBaseManagedObject.BaseError.notFound
        }
        
        return storedObject
    }
    
    class func findAll<T : NIBaseManagedObject>(_ type: T.Type, withID uniqueID: Any, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> [T]
    {
        return try self.findAll(type, withValue: uniqueID, forKey: T.uniqueIDProperyName, inManagedObjectContext: managedObjectContext)
    }
    
    class func findAll<T : NIBaseManagedObject>(_ type: T.Type, withValue valueObject : Any? = nil, forKey keyString : String? = nil, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> [T]
    {
        let predicate = self.predicate(forKey: keyString, withValue: valueObject)

        return try self.findAll(type, predicate: predicate, inManagedObjectContext: managedObjectContext)
    }
    
    class func findAll<T : NIBaseManagedObject>(_ type: T.Type, predicate : NSPredicate?, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> [T]
    {
        let fetchRequest = self.fetchRequestForType(type)
        fetchRequest.predicate = predicate
        
        do
        {
            return try managedObjectContext.fetch(fetchRequest)
        }
        catch
        {
            print("error performing fetch request : \(error)")
            throw NIBaseManagedObject.BaseError.coreData
        }
    }
    
    class func count<T : NIBaseManagedObject>(_ type: T.Type, predicate : NSPredicate?, inManagedObjectContext managedObjectContext : NSManagedObjectContext) throws -> Int
    {
        let fetchRequest = self.fetchRequestForType(type)
        fetchRequest.predicate = predicate
        
        do
        {
            return try managedObjectContext.count(for: fetchRequest)
        }
        catch
        {
            print("error performing fetch request : \(error)")
            throw NIBaseManagedObject.BaseError.coreData
        }
    }
    
    class func cleanupDeletions<T : NIBaseManagedObject>(oldObjects : [T], newObjects : [T], inManagedObjectContext managedObjectContext : NSManagedObjectContext)
    {
        for oldObject in oldObjects
        {
            // If old object NOT found in new objects, remove it
            if (!newObjects.contains(oldObject))
            {
                managedObjectContext.delete(oldObject)
            }
        }
    }
    
    class func fetchRequestForType<T : NIBaseManagedObject>(_ type: T.Type) -> NSFetchRequest<T>
    {
        let fetchRequest = NSFetchRequest<T>(entityName: NSStringFromClass(T.self))
        return fetchRequest
    }
    
    private class func predicate(forKey keyString : String? = nil, withValue valueObject : Any? = nil) -> NSPredicate?
    {
        if let value = valueObject, let key = keyString
        {
            switch value
            {
                case let intValue as Int:
                    return NSPredicate(format: "%K = %d", key, intValue)
                
                case let doubleValue as Double:
                    return NSPredicate(format: "%K = %f", key, doubleValue)
                
                case let stringValue as String:
                    return  NSPredicate(format: "%K = %@", key, stringValue)
                
                case let nsObjectValue as NSObject:
                    return NSPredicate(format: "%K = %@", key, nsObjectValue)
                
                default:
                    fatalError("What are you trying to pass as the value?")
            }
        }

        return nil
    }
    
// MARK: Search in collection
//    
//    class func findFirst<T : NIBaseManagedObject>(type: T.Type, withID uniqueID: Any, inArray array : [T]) throws -> T
//    {
//        let results = array.filter
//        {
//            (object : T) -> Bool in
//            
//            guard let value = (object as NSManagedObject).value(forKey: T.uniqueIDProperyName) as? NSObject, (value == uniqueID) else
//            {
//                return false
//            }
//            
//            return true
//        }
//        
//        guard let storedObject = results.last else
//        {
//            throw NIBaseManagedObject.BaseError.notFound
//        }
//        
//        return storedObject
//    }
}
