//
//  House+CoreDataClass.swift
//

import Foundation
import CoreData

@objc(House)
class House: NIBaseManagedObject {
    struct Constant {
        static let number = "number"
        static let collected = "collected"
    }
    
    override class var uniqueIDProperyName: String {
        return Constant.number
    }
    
    class var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: Constant.number, ascending: false)]
    }
    
    class var myPredicate: NSPredicate {
        return NSPredicate(format: "%K > 0", Constant.collected)
    }
    
    class var duplicatesPredicate: NSPredicate {
        return NSPredicate(format: "%K > 1", Constant.collected)
    }
    
    class var missingPredicate: NSPredicate {
        return NSPredicate(format: "%K == 0", Constant.collected)
    }
}
