//
//  HouseFilter.swift
//

import CoreData

enum HouseFilter {
    case collection
    case my
    case duplicates
    case missing
    
    var fetchRequest: NSFetchRequest<House> {
        let request = NIManagedObjectManager.fetchRequestForType(House.self)
        request.sortDescriptors = House.defaultSortDescriptors
        request.predicate = self.predicate
        
        return request
    }
    
    private var predicate: NSPredicate? {
        switch (self) {
            case .collection:   return nil
            case .my:           return House.myPredicate
            case .duplicates:   return House.duplicatesPredicate
            case .missing:      return House.missingPredicate
        }
    }
    
    static var allFilers: [HouseFilter] {
        return [.collection, .my, .duplicates, .missing]
    }
    
    static var initial: HouseFilter {
        return .collection
    }
}

extension HouseFilter: SegmentedControlItem {
    var title: String {
        switch (self) {
            case .collection:   return "Collection"
            case .my:           return "My houses"
            case .duplicates:   return "Duplicates"
            case .missing:      return "Missing"
        }
    }
}
