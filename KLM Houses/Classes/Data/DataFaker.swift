//
//  DataFaker.swift
//

import CoreData

class DataFaker {
    class func generateFakeHouses(count: Int, inManagedObjectContext managedObjectContext: NSManagedObjectContext) {
        for i in (1..<count) {
            guard let house = try? NIManagedObjectManager.create(House.self, inManagedObjectContext: managedObjectContext) else {
                fatalError("Cannot create House")
            }
            
            house.number = Int16(i)
            house.name = "NAME: \(i)"
            house.collected = Int32(random(in: (0...5)))
            house.address = "some address"
            house.info = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
//            house.text = "text: \(i)"
//            house.imageURL = "miniature\(i)"
        }
    }
}
