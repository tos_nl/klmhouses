//
//  House+CoreDataProperties.swift
//

import Foundation
import CoreData


extension House {
    @NSManaged var address: String?
    @NSManaged var collected: Int32
    @NSManaged var comment: String?
    @NSManaged var image: String?
    @NSManaged var info: String?
    @NSManaged var latitude: Double
    @NSManaged var longitude: Double
    @NSManaged var name: String?
    @NSManaged var number: Int16
    @NSManaged var rating: Int16
}
