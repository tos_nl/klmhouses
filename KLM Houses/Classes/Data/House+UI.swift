//
//  House+UI.swift
//

import UIKit

extension House {
    var tableImage: UIImage? {
        return UIImage(named: "table-\(self.number)")
    }
    
    var miniatureImage: UIImage? {
        return UIImage(named: "miniature\(self.number)")
    }
    
    var realImage: UIImage? {
        return UIImage(named: String(format: "real%02d", self.number))
    }
}
