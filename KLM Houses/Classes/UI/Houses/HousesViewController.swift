//
//  HousesViewController.swift
//

import UIKit

class HousesViewController: UIViewController {
    fileprivate struct Constant {
        static let ListCellHeight = CGFloat(80)
        static let CellReuseID = "houseOverviewReuseID"
    }
    
    weak var houseListInteractor: HouseListInteractor?
    
    let filers = HouseFilter.allFilers
    var filter: HouseFilter! {
        didSet {
            self.dataSource = CollectionViewDataSource(fetchRequest: self.filter.fetchRequest)
            self.updateTitle()
        }
    }
    
    var dataSource: CollectionViewDataSource<House>! {
        didSet {
            self.dataSource.delegate = self
            self.dataSource.collectionView = self.collectionView
        }
    }
    
    let gridLayout = HousesGridCollectionViewLayout.defaultVerticalGridLayout
    let listLayout = HousesListCollectionViewLayout(cardHeight: Constant.ListCellHeight)
    
    @IBOutlet private var gridBarButtonItem: UIBarButtonItem!
    @IBOutlet private var listBarButtonItem: UIBarButtonItem!
    
    @IBOutlet fileprivate var segmentsContainerView: UIStackView!
    fileprivate var segmentedControl: SegmentedControl<HouseFilter>!
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var emptyView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.filter = HouseFilter.initial
        self.setCollectionViewLayout(self.gridLayout, modeBarButtonItem: self.listBarButtonItem)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.klm_blue
        
        self.setupSegmentedControl()
        self.updateTitle()
    }
    
    private func setupSegmentedControl() {
        self.collectionView.backgroundColor = UIColor.klm_screenBackground
        self.segmentedControl = SegmentedControl(items: self.filers, canScroll: true)
        self.segmentsContainerView.addArrangedSubview(self.segmentedControl)
        
        self.segmentedControl.selectItem(self.filter, animated: false)
        
        self.segmentedControl.selectionHandler = { [weak self] (oldFilter: HouseFilter?, newFilter: HouseFilter) in
            self?.filter = newFilter
        }
    }
    
    @IBAction private func gridLayoutButtonPressed() {
        self.setCollectionViewLayout(self.gridLayout, modeBarButtonItem: self.listBarButtonItem)
    }
    
    @IBAction private func listLayoutButtonPressed() {
        self.setCollectionViewLayout(self.listLayout, modeBarButtonItem: self.gridBarButtonItem)
    }
    
    private func setCollectionViewLayout(_ layout: HousesCollectionViewLayout, modeBarButtonItem: UIBarButtonItem) {
        guard (self.collectionView.collectionViewLayout != layout) else {
            return
        }
        
        // Snap to first visible cell
        layout.transitionIndexPath = self.collectionView.ni_indexPathForFirstFullyVisibleItem()
        self.collectionView.setCollectionViewLayout(layout, animated: true)
        layout.transitionIndexPath = nil
        self.navigationItem.setRightBarButtonItems([modeBarButtonItem], animated: true)
    }
    
    private func updateTitle() {
        self.navigationItem.title = self.filter.title
    }
}

extension HousesViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.dataSource.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.numberOfItems(in: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.CellReuseID, for: indexPath) as? HouseCollectionViewCell else {
            fatalError("wrong type of cell")
        }
        
        let house = self.dataSource.object(at: indexPath)
        cell.setup(for: house)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.houseListInteractor?.userWantsToViewDetails(ofHouseAt: indexPath, in: self.dataSource, from: self)
    }
}

extension HousesViewController: CollectionViewDataSourceDelegate {
    func collectionViewDataSourceWillRefresh() {
        
    }
    
    func collectionViewDataSourceDidRefresh() {
        let isEmpty = (self.dataSource.fetchedObjects.count == 0)
        
        self.collectionView.isHidden = isEmpty
        self.emptyView.isHidden = !isEmpty
    }
}

