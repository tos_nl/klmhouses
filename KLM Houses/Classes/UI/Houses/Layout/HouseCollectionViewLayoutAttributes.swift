//
//  HouseCollectionViewLayoutAttributes.swift
//

import UIKit

class HouseCollectionViewLayoutAttributes: UICollectionViewLayoutAttributes {
    enum Mode {
        case grid
        case list
    }
    
    var mode = Mode.grid
    
    override func copy(with zone: NSZone?) -> Any {
        let copy = super.copy(with: zone) as! HouseCollectionViewLayoutAttributes
        copy.mode = mode
        
        return copy
    }
    
    override func isEqual(_ object: Any?) -> Bool{
        guard let attributes = object as? HouseCollectionViewLayoutAttributes, (attributes.mode == self.mode) else {
            return false
        }
        
        return super.isEqual(object)
    }
}

class HousesCollectionViewLayout: UICollectionViewFlowLayout {
    var transitionIndexPath: IndexPath?
    
    class var mode: HouseCollectionViewLayoutAttributes.Mode {
        fatalError("should override")
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attribute = super.layoutAttributesForItem(at: indexPath) as? HouseCollectionViewLayoutAttributes else {
            return nil
        }
        
        attribute.mode = type(of: self).mode
        return attribute
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        
        for case let attribute as HouseCollectionViewLayoutAttributes in attributes {
            attribute.mode = type(of: self).mode
        }
        
        return attributes
    }
    
    override class var layoutAttributesClass: Swift.AnyClass {
        return HouseCollectionViewLayoutAttributes.self
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        guard let collectionView = self.collectionView else {
            fatalError("no collectionView")
        }
        
        return (collectionView.bounds.width != newBounds.width) || (collectionView.bounds.height != newBounds.height)
    }
}
