//
//  HousesCollectionViewLayout.swift
//

import UIKit

class HousesGridCollectionViewLayout: HousesCollectionViewLayout {
    var numberOfColumns = 3
    var cardAspectRatio = CGFloat(1)
    
    class var defaultVerticalGridLayout: HousesGridCollectionViewLayout {
        let layout = HousesGridCollectionViewLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsets.zero
        layout.scrollDirection = .vertical
        return layout
    }
    
    override func prepare() {
        guard let collectionView = self.collectionView else {
            fatalError("no collectionView")
        }
        
        super.prepare()
        
        let collectionViewSize = collectionView.bounds.size
        let marginsWidth = self.minimumLineSpacing * (CGFloat(self.numberOfColumns) - 1) + self.sectionInset.left + self.sectionInset.right
        let itemWidth = (collectionViewSize.width - marginsWidth)/CGFloat(self.numberOfColumns)
        let itemHeight = (self.cardAspectRatio > 0.001) ? itemWidth/self.cardAspectRatio : (collectionViewSize.height - self.sectionInset.top - self.sectionInset.bottom)
        
        self.itemSize = CGSize(width: floor(itemWidth), height: floor(itemHeight))
    }
    
    override class var mode: HouseCollectionViewLayoutAttributes.Mode {
        return .grid
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        guard let transitionIndexPath = self.transitionIndexPath,
              let transitionLayoutAttributes = self.layoutAttributesForItem(at: transitionIndexPath) else {
            return proposedContentOffset
        }
        
        return CGPoint(x: 0, y: transitionLayoutAttributes.frame.minY - self.minimumLineSpacing - self.sectionInset.top)
    }
}

