//
//  HousesListCollectionViewLayout.swift
//

import UIKit

class HousesListCollectionViewLayout: HousesCollectionViewLayout {
    fileprivate struct Constant {
        static let LineSpacing = CGFloat(0)
        static let SectionInsets = UIEdgeInsets.zero
    }
    
    let cardHeight: CGFloat
    
    init(cardHeight: CGFloat = 80) {
        self.cardHeight = cardHeight
        
        super.init()
        
        self.minimumLineSpacing = Constant.LineSpacing
        self.sectionInset = Constant.SectionInsets
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.cardHeight = 80
        
        super.init(coder: aDecoder)
    }
    
    override func prepare() {
        guard let collectionView = self.collectionView else {
            fatalError("no collectionView")
        }
        
        super.prepare()
        
        let collectionViewSize = collectionView.bounds.size
        let marginsWidth = self.sectionInset.left + self.sectionInset.right
        let itemWidth = (collectionViewSize.width - marginsWidth)
        
        self.itemSize = CGSize(width: floor(itemWidth), height: self.cardHeight)
        self.minimumInteritemSpacing = 0
    }
    
    override class var mode: HouseCollectionViewLayoutAttributes.Mode {
        return .list
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        guard let transitionIndexPath = self.transitionIndexPath,
              let transitionLayoutAttributes = self.layoutAttributesForItem(at: transitionIndexPath) else {
            return proposedContentOffset
        }
        
        return CGPoint(x: 0, y: transitionLayoutAttributes.frame.minY - self.minimumLineSpacing - self.sectionInset.top)
    }
}
