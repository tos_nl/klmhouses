//
//  HouseCollectionViewCell.swift
//

import UIKit
//import AlamofireImage

class HouseCollectionViewCell: UICollectionViewCell {
    struct Constant {
        static let placeholder = UIImage(named: "iconPlaceHolder")
        static let checkmark = UIImage(named: "tickMark_Blue")
    }
    
    @IBOutlet private var contentStackView: UIStackView!
    @IBOutlet private var numberStackView: UIStackView!
    @IBOutlet private var detailsStackView: UIStackView!
    
    @IBOutlet private var imageView: UIImageView!
    
    @IBOutlet private var numberLabel: UILabel!
    @IBOutlet private var collectedImageView: UIImageView!
    
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectedImageView.image = Constant.checkmark
        
        self.contentView.layer.borderWidth = 0.5/UIScreen.main.scale
        self.contentView.layer.borderColor = UIColor.klm_blue.cgColor
        
        self.updateMode()
    }
    
    private var mode = HouseCollectionViewLayoutAttributes.Mode.grid {
        didSet {
            self.updateMode()
        }
    }
    
    func setup(for house: House) {
        self.numberLabel.text = "\(house.number)"
        self.collectedImageView.isHidden = (house.collected <= 0)
        
        self.imageView.image = house.miniatureImage ?? Constant.placeholder
        self.nameLabel.text = house.name
        self.addressLabel.text = house.address
     }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        
        guard let layoutAttributes = layoutAttributes as? HouseCollectionViewLayoutAttributes, layoutAttributes.mode != self.mode else {
            // Nothing to do, we are in the same mode
            return
        }
        
        self.mode = layoutAttributes.mode
    }
    
    private func updateMode() {
        let isInGridMode = (self.mode == .grid)
        
        if (isInGridMode) {
            self.numberLabel.textAlignment = .center
            self.nameLabel.isHidden = true
            self.addressLabel.isHidden = true
            
            self.contentStackView.axis = .vertical
            self.detailsStackView.isHidden = true
        }
        else {
            self.numberLabel.textAlignment = .left
            self.nameLabel.isHidden = false
            self.addressLabel.isHidden = false
            
            self.contentStackView.axis = .horizontal
            self.detailsStackView.isHidden = false
        }
    }
}
