//
//  UIColor+KLM.swift
//

import UIKit

extension UIColor {
    static var klm_blue: UIColor {
        return UIColor(red: 0.0, green: 0.59, blue: 0.85, alpha: 1)
    }
    
    static var klm_screenBackground: UIColor {
        return UIColor(red: 0.93, green: 0.93, blue: 0.92, alpha: 1)
    }
}
