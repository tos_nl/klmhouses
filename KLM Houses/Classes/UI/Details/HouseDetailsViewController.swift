//
//  HouseDetailsViewController.swift
//

import UIKit
import CoreLocation

class HouseDetailsViewController: UITableViewController {
    var house: House!
    let rows = Row.allRows
    
    weak var houseInteractor: HouseInteractor?
    
    let locationManager = CLLocationManager()
    var location: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.delegate = self
        
        guard let _ = self.house else {
            fatalError("no house set")
        }
        
        self.title = NSLocalizedString("Details", comment: "HouseDetailsViewController.Title")
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rows.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modelRow = self.rows[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: modelRow.cellID),
              let baseCell = cell as? HouseDetailsCell else {
            fatalError("no such cell")
        }
        
        baseCell.setup(for: self.house, houseInteractor: self.houseInteractor)
        return cell
    }
}

extension HouseDetailsViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let oldLocation = self.location
        self.location = locations.last
        
        if (oldLocation == nil) && (self.location != nil) {
            self.tableView.reloadData()
        }
    }
}
