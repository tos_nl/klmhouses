//
//  HouseDetailsInfoTableViewCell.swift
//

import UIKit

class HouseDetailsInfoTableViewCell: UITableViewCell, HouseDetailsCell {

    @IBOutlet private var infoLabel: UILabel!

    func setup(for house: House, houseInteractor: HouseInteractor?) {
        self.infoLabel.text = house.info
    }
}
