//
//  HouseDetailsImagesTableViewCell.swift
//

import UIKit

class HouseDetailsImagesTableViewCell: UITableViewCell, HouseDetailsCell {
    struct Constant {
        static let placeholder = UIImage(named: "iconPlaceHolder")
    }
    
    @IBOutlet private var miniatureImageView: UIImageView!
    @IBOutlet private var realImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.klm_blue
    }

    func setup(for house: House, houseInteractor: HouseInteractor?) {
        self.miniatureImageView.image = house.miniatureImage ?? Constant.placeholder
        self.realImageView.image = house.realImage ?? Constant.placeholder
    }

}
