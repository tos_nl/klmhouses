//
//  HouseDetailsCollectionTableViewCell.swift
//

import UIKit

class HouseDetailsCollectionTableViewCell: UITableViewCell, HouseDetailsCell {
    
    private var observation: NSKeyValueObservation?
    private var house: House?
    private weak var houseInteractor: HouseInteractor?
    
    @IBOutlet private var buttonsContainerView: UIView!
    @IBOutlet private var minusButton: UIButton!
    @IBOutlet private var plusButton: UIButton!
    @IBOutlet private var countLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.buttonsContainerView.layer.borderWidth = 1
        self.buttonsContainerView.layer.cornerRadius = 22
        self.buttonsContainerView.layer.borderColor = UIColor.klm_blue.cgColor
        self.buttonsContainerView.clipsToBounds = true
        
        self.countLabel.backgroundColor = UIColor.klm_blue
        self.setupColors(for: self.minusButton)
        self.setupColors(for: self.plusButton)
    }
    
    private func setupColors(for button: UIButton) {
        button.setTitleColor(UIColor.klm_blue, for: .normal)
        button.setTitleColor(UIColor.klm_blue.withAlphaComponent(0.6), for: .selected)
        button.setTitleColor(UIColor.klm_blue.withAlphaComponent(0.3), for: .disabled)
    }

    func setup(for house: House, houseInteractor: HouseInteractor?) {
        self.house = house
        self.houseInteractor = houseInteractor
        
        self.updateButtonsAndLabel(for: house)
        
        self.observation = house.observe(\.collected) { [weak self] (house: House, _: NSKeyValueObservedChange<Int32>) in
            self?.updateButtonsAndLabel(for: house)
        }
    }
    
    private func updateButtonsAndLabel(for house: House) {
        self.countLabel.text = "\(house.collected)"
        
        self.minusButton.isEnabled = (house.collected > 0)
        self.plusButton.isEnabled = (house.collected < 100)
    }
    
    @IBAction private func minusButtonPressed() {
        guard let house = self.house else {
            fatalError("no house")
        }
        
        self.houseInteractor?.userWantsToDecrementCollected(house: house)
    }
    
    @IBAction private func plusButtonPressed() {
        guard let house = self.house else {
            fatalError("no house")
        }
        
        self.houseInteractor?.userWantsToIncrementCollected(house: house)
    }
}
