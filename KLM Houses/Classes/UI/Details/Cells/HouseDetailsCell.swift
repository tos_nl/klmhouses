//
//  HouseDetailsCell.swift
//

import UIKit

protocol HouseDetailsCell {
    func setup(for house: House, houseInteractor: HouseInteractor?)
}

