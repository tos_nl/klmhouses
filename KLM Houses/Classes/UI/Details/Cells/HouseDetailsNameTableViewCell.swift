//
//  HouseDetailsNameTableViewCell.swift
//

import UIKit

class HouseDetailsNameTableViewCell: UITableViewCell, HouseDetailsCell {
    
    private var observation: NSKeyValueObservation?
    
    @IBOutlet private var circleView: UIView!
    @IBOutlet private var numberLabel: UILabel!
    @IBOutlet private var checkmarkImageView: UIImageView!
    
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var alreadyOwnedLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.circleView.layer.borderWidth = 1
        self.circleView.layer.cornerRadius = 16
        self.circleView.layer.borderColor = UIColor.klm_blue.cgColor
        
        self.numberLabel.textColor = UIColor.klm_blue
        self.nameLabel.textColor = UIColor.klm_blue
    }

    func setup(for house: House, houseInteractor: HouseInteractor?) {
        self.numberLabel.text = "\(house.number)"
        self.nameLabel.text = house.name
        
        self.updateCollectedStatus(for: house)
        
        self.observation = house.observe(\.collected) { [weak self] (house: House, _: NSKeyValueObservedChange<Int32>) in
            self?.updateCollectedStatus(for: house)
        }
    }
    
    private func updateCollectedStatus(for house: House) {
        let isOwned = (house.collected > 0)
        self.checkmarkImageView.isHidden = !isOwned
        self.alreadyOwnedLabel.isHidden = !isOwned
    }
}
