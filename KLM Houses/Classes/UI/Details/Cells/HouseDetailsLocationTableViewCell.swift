//
//  HouseDetailsLocationTableViewCell.swift
//

import UIKit
import MapKit

class HouseDetailsLocationTableViewCell: UITableViewCell, HouseDetailsCell {

    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var mapView: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mapView.isUserInteractionEnabled = false
        self.mapView.showsUserLocation = true
    }
    
    func setup(for house: House, houseInteractor: HouseInteractor?) {
        self.infoLabel.text = house.address
        
        self.setupMapView(for: house)
    }
    
    private func setupMapView(for house: House) {
        let oldAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(oldAnnotations)
        
        let isValidLocation = (house.latitude != 0) && (house.longitude != 0)
        self.mapView.isHidden = !isValidLocation
        
        guard isValidLocation else {
            return
        }
        
        let houseCoordinate = CLLocationCoordinate2D(latitude: house.latitude, longitude: house.longitude)
        let annotation = MKPointAnnotation()
        annotation.coordinate = houseCoordinate
        self.mapView.showAnnotations([annotation], animated: false)
        
        let coordinates = [self.mapView.userLocation.coordinate, houseCoordinate]
        let zoomRect = coordinates.reduce(MKMapRectNull) { (mapRect: MKMapRect, coordinate: CLLocationCoordinate2D) -> MKMapRect in
            let currentPoint = MKMapPointForCoordinate(coordinate)
            let currentRect = MKMapRectMake(currentPoint.x, currentPoint.y, 0, 0)
            
            return MKMapRectUnion(mapRect, currentRect)
        }
        
        self.mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50), animated: false)
    }
}

extension HouseDetailsLocationTableViewCell: MKMapViewDelegate {
    fileprivate struct MapConstant {
        static let AnnotationViewReuseID = "annotationID"
        static let AnnotationImage = #imageLiteral(resourceName: "Map_Pin")
        static let AnnotationImageCenterOffset = CGPoint(x: 0, y: -25)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else {
            return nil
        }
        
        guard let reusedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: MapConstant.AnnotationViewReuseID) else {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: MapConstant.AnnotationViewReuseID)
            annotationView.image = MapConstant.AnnotationImage
            annotationView.centerOffset = MapConstant.AnnotationImageCenterOffset
            return annotationView
        }
        
        reusedAnnotationView.annotation = annotation
        return reusedAnnotationView
    }
}
