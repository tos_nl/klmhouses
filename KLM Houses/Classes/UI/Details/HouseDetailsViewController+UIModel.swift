//
//  HouseDetailsViewController+UIModel.swift
//

import UIKit

extension HouseDetailsViewController {
    enum Row {
        case images
        case name
        case info
        case location
        case collection
        
        var cellID: String {
            switch (self) {
                case .images:       return "imagesCellID"
                case .name:         return "nameCellID"
                case .info:         return "infoCellID"
                case .location:     return "locationCellID"
                case .collection:   return "collectionCellID"
            }
        }
        
        static var allRows: [Row] {
            return [.images, .name, .info, .location, .collection]
        }
    }
}
