//
//  DataSourceBatchUpdate.swift
//

import UIKit

enum NIDataSourceChange
{
    case insert(indexPath : IndexPath)
    case delete(oldIndexPath : IndexPath)
    case move(oldIndexPath : IndexPath, indexPath : IndexPath)
    case reload(indexPath : IndexPath)
}

class NIMutableDataSourceBatchUpdate
{
    var insertedSectionChanges = IndexSet()
    var deletedSectionChanges = IndexSet()
    
    var objectChanges = [NIDataSourceChange]()
}
