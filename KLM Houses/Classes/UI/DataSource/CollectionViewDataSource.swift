//
//  CollectionViewDataSource.swift
//

import UIKit
import CoreData

protocol CollectionViewDataSourceDelegate: class
{
    func collectionViewDataSourceWillRefresh()
    func collectionViewDataSourceDidRefresh()
}

// When subclassing NSObject, it crashes compiler with segmentation fault
// That's why CollectionViewDataSourceDelegate is added
class CollectionViewDataSource<T: NSManagedObject>
{
    lazy var delegateProxy: FetchedResultsControllerDelegateProxy =
    {
        let delegateProxy = FetchedResultsControllerDelegateProxy()
        delegateProxy.delegate = self
        return delegateProxy
    }()
    
    weak var delegate: CollectionViewDataSourceDelegate?
    
    init(other: CollectionViewDataSource<T>)
    {
        self.fetchRequest = other.fetchRequest
    }
    
    init(fetchRequest: NSFetchRequest<T>)
    {
        self.fetchRequest = fetchRequest
    }
    
    var collectionView : UICollectionView?
    {
        didSet
        {
            self.performFetchIfNecessary()
        }
    }
    
    fileprivate var currentUpdate: NIMutableDataSourceBatchUpdate?
    
    private var shouldPerformFetch = true
    private var fetchRequest: NSFetchRequest<T>
    private var privateFetchedResultsController : NSFetchedResultsController<T>?
    private var fetchedResultsController : NSFetchedResultsController<T>
    {
        guard let privateFetchedResultsController = self.privateFetchedResultsController else
        {
            let fetchedResultsController = NSFetchedResultsController(fetchRequest: self.fetchRequest, managedObjectContext: Storage.instance.mainQueueContext, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController.delegate = self.delegateProxy
            self.privateFetchedResultsController = fetchedResultsController
            return fetchedResultsController
        }
        
        return privateFetchedResultsController
    }
    
    func reload()
    {
        guard let privateFetchedResultsController = self.privateFetchedResultsController else
        {
            return
        }
        
        self.contentWillChange()
        
        privateFetchedResultsController.delegate = nil
        self.privateFetchedResultsController = nil
        
        self.performFetch()
        
        self.collectionView?.reloadData()
        
        self.contentDidChange()
    }
    
    fileprivate func contentWillChange()
    {
        self.delegate?.collectionViewDataSourceWillRefresh()
    }
    
    fileprivate func contentDidChange()
    {
        self.delegate?.collectionViewDataSourceDidRefresh()
    }
    
    fileprivate func performFetch()
    {
        do
        {
            try self.fetchedResultsController.performFetch()
        }
        catch (let error)
        {
            print("error performing fetch : \(error)")
        }
    }
    
    fileprivate func performFetchIfNecessary()
    {
        guard (self.shouldPerformFetch) else
        {
            return
        }
        
        self.shouldPerformFetch = false
        
        self.contentWillChange()
        self.performFetch()
        self.contentDidChange()
        
        self.collectionView?.reloadData()
    }
    
    func object(at indexPath: IndexPath) -> T
    {
        return self.fetchedResultsController.object(at: indexPath)
    }
    
    func indexPath(forObject object : T) -> IndexPath?
    {
        return self.fetchedResultsController.indexPath(forObject: object)
    }
    
    func numberOfSections() -> Int
    {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func numberOfItems(in section: Int) -> Int
    {
        return self.fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    var fetchedObjects: [T]
    {
        return self.fetchedResultsController.fetchedObjects ?? []
    }
}

extension CollectionViewDataSource: FetchedResultsControllerDelegate
{
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
    {
        guard let currentUpdate = self.currentUpdate else
        {
            fatalError("no currentUpdate")
        }
        
        switch (type)
        {
            case .insert:
                guard let newIndexPath = newIndexPath else
                {
                    fatalError("no newIndexPath")
                }
                
                currentUpdate.objectChanges.append(.insert(indexPath: newIndexPath))
            
            case .delete:
                guard let indexPath = indexPath else
                {
                    fatalError("no indexPath")
                }
                
                currentUpdate.objectChanges.append(.delete(oldIndexPath: indexPath))
            
            case .update:
                guard let indexPath = indexPath else
                {
                    fatalError("no indexPath")
                }
                
                currentUpdate.objectChanges.append(.reload(indexPath: indexPath))
            
            case .move:
                guard let indexPath = indexPath,
                      let newIndexPath = newIndexPath else
                {
                    fatalError("no indexPath or newIndexPath")
                }
                
                if (indexPath != newIndexPath)
                {
                    currentUpdate.objectChanges.append(.move(oldIndexPath: indexPath, indexPath: newIndexPath))
                }
                else
                {
                    currentUpdate.objectChanges.append(.reload(indexPath: newIndexPath))
                }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType)
    {
        guard let currentUpdate = self.currentUpdate else
        {
            fatalError("no currentUpdate")
        }
        
        switch (type)
        {
            case .insert:
                currentUpdate.insertedSectionChanges.update(with: sectionIndex)
            
            case .delete:
                currentUpdate.deletedSectionChanges.update(with: sectionIndex)
            
            default: ()
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
    {
        self.contentWillChange()
        self.currentUpdate = NIMutableDataSourceBatchUpdate()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
    {
        guard let collectionView = self.collectionView,
              let currentUpdate = self.currentUpdate else
        {
            fatalError("no collectionView or no currentUpdate")
        }
        
        collectionView.performBatchUpdates(
        {
            for objectChange in currentUpdate.objectChanges
            {
                switch (objectChange)
                {
                    case .insert(let indexPath):
                        collectionView.insertItems(at: [indexPath])
                    
                    case .delete(let oldIndexPath):
                        collectionView.deleteItems(at: [oldIndexPath])
                    
                    case .move(let oldIndexPath, let indexPath):
                        collectionView.moveItem(at: oldIndexPath, to: indexPath)
                    
                    case .reload(let indexPath):
                        collectionView.reloadItems(at: [indexPath])
                }
            }
            
            collectionView.insertSections(currentUpdate.insertedSectionChanges)
            collectionView.deleteSections(currentUpdate.deletedSectionChanges)
            
        }, completion:
        {
            (finished: Bool) in
            
            self.currentUpdate = nil
            self.contentDidChange()
        })
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String?
    {
        return nil
    }
}
