//
//  CollectionViewFlowLayout.swift
//

import UIKit

public class CollectionViewFlowLayout: UICollectionViewFlowLayout {
    override public func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        guard let collectionView = self.collectionView, newBounds.size != collectionView.bounds.size else {
            return false
        }
        
        return true
    }
    
    override public func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let invalidationContext = UICollectionViewFlowLayoutInvalidationContext()
        
        if let collectionView = self.collectionView, newBounds.size != collectionView.bounds.size {
            invalidationContext.invalidateFlowLayoutDelegateMetrics = true
        }
        
        return invalidationContext
    }
}
