//
//  SegmentedControlCell.swift
//

import UIKit

public class SegmentedControlCell: UICollectionViewCell
{
    private struct Constant
    {
        static let TextInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        static let MinimumTextWidth = CGFloat(40)
    }
    
    @IBOutlet private (set) var selectionView: UIView!
    @IBOutlet private (set) var textLabel: UILabel!
    
    private var item: SegmentedControlItem?
    
    override public func awakeFromNib()
    {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.selectionView.backgroundColor = UIColor.klm_blue
        self.textLabel.textColor = UIColor.klm_blue
    }
    
    public func setup(for item: SegmentedControlItem)
    {
        self.item = item
        self.textLabel.text = item.title
        self.updateColors(selected: self.isSelected)
    }
    
    public func updateColors(selected: Bool)
    {
        self.selectionView.alpha = selected ? 1 : 0
    }
    
    override public var isSelected: Bool
    {
        didSet
        {
            self.updateColors(selected: self.isSelected)
        }
    }
    
    class func minimumWidth(for item: SegmentedControlItem) -> CGFloat
    {
        return self.size(of: item.title)
    }
    
    class func size(of text: String) -> CGFloat
    {
        let size = text.boundingRect(with: CGSize(width: CGFloat.infinity, height: CGFloat.infinity), options: [], context: nil)
        let width = Constant.TextInsets.left + ceil(size.width) + Constant.TextInsets.right
        
        return max(width, Constant.MinimumTextWidth)
    }
}
