//
//  SegmentedControl.swift
//

import UIKit

public protocol SegmentedControlItem {
    var title: String { get }
}

fileprivate struct Constant {
    static let SubcellID = "segmentedControlSubcellID"
    static let SubcellNibName = "SegmentedControlCell"
}

public class SegmentedControl<T: SegmentedControlItem & Equatable>: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    public typealias SelectionHandler = (T?, T) -> ()
    public var selectionHandler: SelectionHandler?
    
    public var items: [T] {
        didSet {
            self.selectedItem = self.items.first
            self.reloadData()
        }
    }
    
    var selectedItem: T?
    private let canScroll: Bool
    
    private lazy var collectionView: UICollectionView = {
        let layout = CollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        layout.scrollDirection = .horizontal
        return UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    }()
    
    required public init(items: [T], canScroll: Bool) {
        self.canScroll = canScroll
        self.items = items
        
        super.init(frame: CGRect.zero)

        self.setupCollectionView()
        
        if let firstItem = items.first {
            self.selectItem(firstItem, animated: false)
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCollectionView() {
        self.collectionView.backgroundColor = UIColor.clear
        
        self.collectionView.register(UINib(nibName: Constant.SubcellNibName, bundle: nil), forCellWithReuseIdentifier: Constant.SubcellID)
        
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.collectionView)
        
        let views = ["collectionView" : self.collectionView] as [String: Any]

        var constraints: [NSLayoutConstraint] = []
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "|[collectionView]|", options: [], metrics: nil, views: views))
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|[collectionView]|", options: [], metrics: nil, views: views))
        NSLayoutConstraint.activate(constraints)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    public func selectItem(_ item: T, animated: Bool) {
        guard (self.selectedItem != item) else {
            return
        }
        
        self.selectedItem = item
        
        if let indexPathToSelect = self.indexPath(for: item) {
            self.collectionView.selectItem(at: indexPathToSelect, animated: animated, scrollPosition: .centeredHorizontally)
        }
    }
    
    public func reloadData() {
        let contentOffset = self.collectionView.contentOffset
        
        UIView.performWithoutAnimation {
            self.collectionView.reloadData()
            self.collectionView.layoutSubviews()
        }
        
        if let selectedIndexPath = self.indexPath(for: self.selectedItem) {
            self.collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .left)
        }
        
        self.collectionView.contentOffset = contentOffset
    }
    
    fileprivate func indexPath(for item: T?) -> IndexPath? {
        guard let item = item, let index = self.items.index(of: item) else
        {
            return nil
        }
        
        return IndexPath(row: index, section: 0)
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard self.canScroll else {
            return CGSize.zero
        }
        
        let collectionViewSize = collectionView.bounds.size
        let item = self.items[indexPath.row]
        return CGSize(width: SegmentedControlCell.minimumWidth(for: item), height: collectionViewSize.height)
    }
    
    //MARK: - UICollectionViewDataSource
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.performWithoutAnimation {
            cell.setNeedsLayout()
            cell.layoutSubviews()
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.items[indexPath.row]
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.SubcellID, for: indexPath) as? SegmentedControlCell else {
            fatalError("wrong type of cell")
        }
        
        cell.setup(for: item)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let oldSelectedItem = self.selectedItem, let oldSelectedIndexPath = self.indexPath(for: oldSelectedItem), oldSelectedIndexPath != indexPath {
            collectionView.deselectItem(at: oldSelectedIndexPath, animated: false)
        }
        
        let oldSelectedItem = self.selectedItem
        let newItem = self.items[indexPath.row]
        
        self.selectedItem = newItem
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        if (oldSelectedItem != newItem) {
            self.selectionHandler?(oldSelectedItem, newItem)
        }
    }
}

