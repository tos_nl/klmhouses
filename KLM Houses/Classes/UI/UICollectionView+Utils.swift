//
//  CollectionViewCell.swift
//

import UIKit

extension UICollectionView
{
    func ni_indexPathForFirstFullyVisibleItem() -> IndexPath?
    {
        let rect = self.bounds
        
        guard let layoutAttributes = self.collectionViewLayout.layoutAttributesForElements(in: rect) else
        {
            return nil
        }
        
        let fullyVisibleAttributes = layoutAttributes.filter
        {
            (layoutAttribute: UICollectionViewLayoutAttributes) -> Bool in
            
            return (layoutAttribute.representedElementCategory == .cell) && rect.contains(layoutAttribute.frame)
        }
        
        let firstFullyVisibleAttribute = fullyVisibleAttributes.min { $0.indexPath < $1.indexPath }
        return firstFullyVisibleAttribute?.indexPath
    }
    
}
