//
//  ViewController.swift
//

import UIKit

protocol HouseListInteractor: class {
    func userWantsToViewDetails(ofHouseAt indexPath: IndexPath, in dataSource: CollectionViewDataSource<House>, from viewController: UIViewController)
}

protocol HouseInteractor: class {
    func userWantsToDecrementCollected(house: House)
    func userWantsToIncrementCollected(house: House)
}

class MasterCoordinator {
    fileprivate struct Constant {
        static let FiltersViewControllerID = "filtersView"
        static let HousesDetailsViewControllerID = "houseDetailsID"
    }
    
    fileprivate let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    fileprivate let housesViewController: HousesViewController
    
    init(window: UIWindow) {
        guard let navigationController = self.mainStoryboard.instantiateInitialViewController() as? UINavigationController,
              let housesViewController = navigationController.viewControllers.first as? HousesViewController else
        {
            fatalError("Can not instantiate HousesViewController")
        }
        
        if let path = Bundle.main.path(forResource: "Miniatures", ofType: "plist"),
           let array = NSDictionary(contentsOfFile: path) as? [String: Any] {
            let houses = HousesParser.parseHouses(from: array, to: Storage.instance.masterManagedObjectContext)
            
            // Preload images
            for house in houses {
                let _ = house.miniatureImage
            }
        }
        
        Storage.instance.masterManagedObjectContext.ni_saveRecursively()
        
        self.housesViewController = housesViewController
        housesViewController.houseListInteractor = self
        
        UIView.transition(with: window, duration: 0.5, options: .transitionCrossDissolve, animations:
        {
            window.rootViewController = navigationController
        }, completion: nil)
    }
}

extension MasterCoordinator: HouseListInteractor {
    func userWantsToViewDetails(ofHouseAt indexPath: IndexPath, in dataSource: CollectionViewDataSource<House>, from viewController: UIViewController) {
        guard let houseDetailsViewController = self.mainStoryboard.instantiateViewController(withIdentifier: Constant.HousesDetailsViewControllerID) as? HouseDetailsViewController else {
            fatalError("Can not instantiate HouseDetailsViewController")
        }
        
        guard let navigationController = viewController.navigationController else {
            fatalError("No navigation controller to push HouseDetailsViewController")
        }
        
        let house = dataSource.fetchedObjects[indexPath.row]
        houseDetailsViewController.houseInteractor = self
        houseDetailsViewController.house = house
        navigationController.pushViewController(houseDetailsViewController, animated: true)
    }
}

extension MasterCoordinator: HouseInteractor {
    func userWantsToDecrementCollected(house: House) {
        self.setCollectedCount(for: house, to: house.collected - 1)
    }
    
    func userWantsToIncrementCollected(house: House) {
        self.setCollectedCount(for: house, to: house.collected + 1)
    }
    
    private func setCollectedCount(for house: House, to collected: Int32) {
        house.collected = collected
        house.managedObjectContext?.ni_saveRecursively()
    }
}
